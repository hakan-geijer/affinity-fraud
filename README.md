# Affinity Fraud and Exploitable Empathy

A zine that explores how the anti-racist, anti-sexist, and other compensatory measures radicals employ to counter bigotry can be exploited my malicious actors to disrupt out ability to organize.

## (Anti-)Copyright

This zine and repo are public domain under a CC0 license.
