DEFAULT_GOAL := help
OPEN = $(word 1, $(wildcard /usr/bin/xdg-open /usr/bin/open))
TARGET := affinity-fraud
TARGET_IMPOSED = $(TARGET)_imposed

IMAGE_DIR := ./img/bin
RAW_IMAGE_DIR := ./data/img

.PHONY: help
help: ## Print the help message
	@awk 'BEGIN {FS = ":.*?## "} /^[0-9a-zA-Z_-]+:.*?## / {printf "\033[36m%s\033[0m : %s\n", $$1, $$2}' $(MAKEFILE_LIST) | \
		sort | \
		column -s ':' -t

$(IMAGE_DIR):
	mkdir $(IMAGE_DIR)

# TODO this target breaks the timestamps needed for `make` to cache the build
images: $(IMAGE_DIR) $(patsubst $(RAW_IMAGE_DIR)/%.png,$(IMAGE_DIR)/%.png,$(wildcard $(RAW_IMAGE_DIR)/*.png))

# this size for this are (rounded for niceness) 300dpi on A5 paper
$(IMAGE_DIR)/%.png: $(RAW_IMAGE_DIR)/%.png
	convert $< -resize 1750x2500 $@ && \
		exiftool -overwrite_original -all= $@

.PHONY: pdf
pdf: $(TARGET).pdf ## Create the plain PDF

.PHONY: pdf-imposed
pdf-imposed: $(TARGET_IMPOSED).pdf ## Create the imposed PDF

.PHONY: $(TARGET).pdf
$(TARGET).pdf: images
	xelatex -file-line-error -halt-on-error -shell-escape -interaction batchmode $(TARGET).tex

.PHONY: $(TARGET_IMPOSED).pdf
$(TARGET_IMPOSED).pdf:
	pdfbook2 -n -p a4paper -s $(TARGET).pdf && \
		mv $(TARGET)-book.pdf $(TARGET_IMPOSED).pdf

.PHONY: open
open: ## Open the PDF
	xdg-open $(TARGET).pdf
